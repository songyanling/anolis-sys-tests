# -*- encoding: utf-8 -*-

"""
@File:      sysinfo.py
@Time:      2022/08/15 16:18:47
@Author:    zhangtaibo.ztb
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
"""

from common.hosts import LocalHost, RemoteHost

class SysInfo():
    """
    Common def for geting OS system info.
    """

    def __init__(self):
        self.remote = RemoteHost(host=self.params.get('remote'),
                                 username=self.params.get('username'),
                                 port=self.params.get('port', default=22),
                                 key=self.params.get('key', default=None),
                                 password=self.params.get('password'))
        self.local = LocalHost()

    def cmd(self, command, host, ignore_status=False):
        if host == 'remote':
            return self.remote.cmd(command, ignore_status=ignore_status)
        elif host == 'local':
            return self.local.cmd(command, ignore_status=ignore_status)

    def get_os_version(self, host="local"):
        ret_c, ret_o = self.cmd('cat /etc/os-release | grep ID= | grep -v platform |awk -F "\\\"" "{print \$2}"|xargs', host)
        return ret_o
    
    def skip_arch(self, arch):
        _, ret_o = self.cmd("arch")
        if ret_o == arch:
            self.skip("Not support for %s arch" % arch)
