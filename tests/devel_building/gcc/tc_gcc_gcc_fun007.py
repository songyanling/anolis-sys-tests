# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_gcc_fun007.py
@Time:      Thu Sep 21 2023 11:01:03
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_gcc_fun007.yaml for details

    :avocado: tags=P1,noarch,local,gcc
    """
    PARAM_DIC = {"pkg_name": "gcc libstdc++-static"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y glibc-static")
        self.cmd("mkdir test person")
        cmdline='''cat >test/main.cpp<<EOF
#include <iostream>
#include "person.h"
using namespace std;
int main(int argc,char* argv[])
{
person p;
cout<<"test g++"<<endl;
return 0;
}
EOF'''
        self.cmd(cmdline)
        cmdline='''cat >person/person.h<<EOF
class person
{
public:
person();
};
EOF'''
        self.cmd(cmdline)
        cmdline='''cat >person/person.cpp<<EOF
#include "person.h"
#include "stdio.h"
person::person()
{
printf("create person");
}
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("cd person;g++ -c person.cpp")
        code,gcc_result=self.cmd("ls person")
        self.assertIn("person.o",gcc_result)
        self.cmd("cd test;g++ main.cpp ../person/person.o -I ../person/ -o main")
        code,gcc_result=self.cmd("ls test")
        self.assertIn("main",gcc_result)
        code,gcc_result=self.cmd("cd test;./main")
        self.assertIn("create person",gcc_result)
        self.assertIn("test g++",gcc_result)
        code,gcc_result=self.cmd("cd test;ldd main")
        self.assertIn("linux-vdso.so.1",gcc_result)
        self.cmd("cd test;g++ main.cpp ../person/person.o -o main_static -I ../person/ -static")
        code,gcc_result=self.cmd("ls test")
        self.assertIn("main_static",gcc_result)
        code,gcc_result=self.cmd("cd test;ldd main_static",ignore_status=True)
        self.assertEqual(1,code)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test person main person.o main_static")