# -*- encoding: utf-8 -*-

"""
@File:      tc_glibc_glibc_fun001.py
@Time:      2023/1/15 21:57:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_glibc_glibc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,glibc
    """
    PARAM_DIC = {"pkg_name": "glibc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y glibc")
        self.cmd("touch glibc-version.c")
        self.cmd("echo -e '#include <stdio.h>\n#include <stdlib.h>\n#include <gnu/libc-version.h>\nint main(int argc, char *argv[])\n{\nprintf(\"GNU lib version:%s\",gnu_get_libc_version());\nexit(EXIT_SUCCESS);\n}' >> glibc-version.c")

    def test(self):
        self.cmd("gcc glibc-version.c")
        code,glibc_result=self.cmd("./a.out")
        self.assertIn("GNU lib version",glibc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf glibc-version.c")