# -*- encoding: utf-8 -*-

"""
@File:      tc_gcc_json-c_fun001.py
@Time:      2023/7/6 21:01:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gcc_json-c_fun002.yaml for details

    :avocado: tags=P2,local,gcc,json-c
    """
    PARAM_DIC = {"pkg_name": "json-c json-c-devel make gcc"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline='''cat > json-c_testfile.c <<EOF
#include <stdio.h>
#include "json.h"
int main(int argc,char *argv[])
{
json_object *new_obj;
char *str="{'Lon':'121.42205','Lat':'31.32118'}";
new_obj=json_tokener_parse(str);
printf("%s",json_object_to_json_string(new_obj));
json_object_put(new_obj);
return 0;
}
EOF
'''
        self.cmd(cmdline)

    def test(self):
        self.cmd("gcc -o json-c_testfile json-c_testfile.c -I /usr/include/json-c/ -L /usr/local/bin/ -l json-c")
        code,json_c_result=self.cmd("./json-c_testfile")
        self.assertEqual('{ "Lon": "121.42205", "Lat": "31.32118" }',json_c_result)


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf json-c_testfile json-c_testfile.c")