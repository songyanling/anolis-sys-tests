# -*- encoding: utf-8 -*-

"""
@File:      tc_dos2unix_dos2unix_fun001.py
@Time:      2023/1/13 10:15:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_dos2unix_dos2unix_fun001.yaml for details

    :avocado: tags=P1,noarch,local,diffstat
    """
    PARAM_DIC = {"pkg_name": "dos2unix"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y dos2unix")
        self.cmd("touch dos2unix_test.sh")
        self.cmd("echo #!/bin/bash >> dos2unix_test.sh")
        self.cmd("echo echo \"hello world\" >> dos2unix_test.sh")
        self.cmd("cp dos2unix_test.sh dos2unix_test1.sh")
        self.cmd("cp dos2unix_test.sh dos2unix_test2.sh")
        self.cmd("cp dos2unix_test.sh dos2unix_test3.sh")

    def test(self):
        self.cmd("dos2unix -i dos2unix_test.sh")
        self.cmd("dos2unix -k dos2unix_test.sh")
        self.cmd("dos2unix -q dos2unix_test.sh")
        self.cmd("dos2unix -o dos2unix_test.sh")
        self.cmd("dos2unix -n dos2unix_test.sh dos2unix_test_hello.sh")
        code,dos2unix_ls_result=self.cmd("ls -l")
        self.assertIn("dos2unix_test_hello.sh",dos2unix_ls_result)
        self.cmd("dos2unix -o dos2unix_test1.sh dos2unix_test2.sh dos2unix_test3.sh")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf dos2unix_test.sh dos2unix_test1.sh dos2unix_test2.sh dos2unix_test3.sh dos2unix_hello.sh")