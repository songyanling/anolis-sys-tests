# -*- encoding: utf-8 -*-

"""
@File:      tc_sed_sed_fun001.py
@Time:      2023/4/10 15:47:20
@Author:    chengweibin
@Version:   1.0
@Contact:   chengweibin@uniontech.com
@License:   Mulan PSL v2
@Modify:    chengweibin
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_sed_sed_fun_001 for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "sed"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('sed --help')
        self.assertIn('sed', result)
        cmd = '''cat >  sedfile << EOF
test123
test456
EOF'''
        self.cmd(cmd)
        self.cmd('sed -i s/test123/hello/g  sedfile')
        code,result = self.cmd('cat sedfile')
        self.assertIn('hello', result)
        self.cmd("sed -i '1,2d' sedfile")
        code,result = self.cmd('cat sedfile')
        self.assertNotIn('test123', result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f sedfile")


