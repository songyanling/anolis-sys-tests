# -*- encoding: utf-8 -*-

"""
@File:      tc_bzip2_bzip2_fun001.py
@Time:      2023/1/12 15:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_bzip2_bzip2_fun001.yaml for details

    :avocado: tags=P1,noarch,local,bzip2
    """
    PARAM_DIC = {"pkg_name": "bzip2"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y bzip2")
        self.cmd("touch bzip2_testfile")
        self.cmd("echo 'bzip2 test' > bzip2_testfile")

    def test(self):
        self.cmd("bzip2 bzip2_testfile")
        code,bzip2_ls_result=self.cmd("ls")
        self.assertIn("bzip2_testfile.bz2",bzip2_ls_result)
        self.cmd("bzip2 -d bzip2_testfile.bz2")
        code,bzip2_ls_result=self.cmd("ls")
        self.assertIn("bzip2_testfile",bzip2_ls_result)
        self.assertNotIn("bzip2_testfile.bz2",bzip2_ls_result)
        code,bzip2_cat_result=self.cmd("cat bzip2_testfile")
        self.assertEquals("bzip2 test",bzip2_cat_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf bzip2_testfile bzip2_testfile.bz2")