# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_ls_fun001.py
@Time:      2022/12/27 17:34:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_ls_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch ls_testfile")
        self.cmd("echo 'ls test' > ls_testfile")

    def test(self):
        code,ls_result=self.cmd("ls -a")
        self.assertIn("ls_testfile",ls_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm ls_testfile")