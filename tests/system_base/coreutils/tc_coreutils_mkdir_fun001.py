# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_mkdir_fun001.py
@Time:      2023/01/04 17:43:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_mkdir_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")

    def test(self):
        self.cmd("mkdir mkdir_testdir")
        code,cmkdir_result=self.cmd("ls -a")
        self.assertIn("mkdir_testdir",cmkdir_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf mkdir_testdir")