# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_touch_fun001.py
@Time:      2022/12/29 11:08:10
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_touch_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gzip
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")

    def test(self):
        self.cmd("touch touch_testfile")
        code,result=self.cmd("ls")
        self.assertIn("touch_testfile",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm touch_testfile")