# -*- encoding: utf-8 -*-

"""
@File:      tc_coreutils_wc_fun001.py
@Time:      2023/01/04 18:31:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_coreutils_wc_fun001.yaml for details

    :avocado: tags=P1,noarch,local,coreutils
    """
    PARAM_DIC = {"pkg_name": "coreutils"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y coreutils")
        self.cmd("touch wc_testfile")
        for i in range(1,21):
            self.cmd("echo 'wc test'{} >> wc_testfile".format(str(i)))

    def test(self):
        code,wc_result=self.cmd("wc -l wc_testfile")
        self.assertIn("20",wc_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f wc_testfile")