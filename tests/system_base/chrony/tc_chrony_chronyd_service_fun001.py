# -*- encoding: utf-8 -*-

"""
@File:      tc_chrony_chronyd.service_fun001.py
@Time:      2022/07/11 14:33:20
@Author:    sunqingwei
@Version:   1.0
@Contact:   sunqingwei@uniontech.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_chrony_chronyd_service_fun001.yaml for details

    :avocado: tags=P1,noarch,local,chrony
    """
    PARAM_DIC = {"pkg_name": "chrony"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y chrony")

    def test(self):
        self.cmd("systemctl start chronyd")
        self.cmd("systemctl status chronyd | grep running")
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
