#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chsh_004.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chsh_fun_004.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux-user"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        shell_list = ['/bin/sh','/bin/bash','/usr/bin/sh','/usr/bin/bash']
        ret_c,ret_o = self.cmd("chsh --list-shells")
        for shell_path in shell_list:
            self.assertTrue(shell_path in ret_o, 'Path does not exist')
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
