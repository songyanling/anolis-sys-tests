#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_flock_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

import os
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_flock_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        os.system("flock  -n --conflict-exit-code 22 ./test.lock -c 'sleep 10' &")
        ret_c, ret_o = self.cmd("flock  -n --conflict-exit-code 22 ./test.lock -c 'sleep 10'",ignore_status=True)
        self.assertTrue(22 == ret_c, 'check output error.')

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf test.lock")
