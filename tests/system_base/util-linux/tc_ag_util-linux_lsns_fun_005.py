#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lsns_005.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lsns_fun_005.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, ret_o = self.cmd("lsns --output NS,TYPE,PID,COMMAND")
        command = ['NS','TYPE','PID','COMMAND','NPROCS','USER']
        for com in command:
            if com == 'NPROCS' or com == 'USER':
                self.assertTrue(com not in ret_o,"check output error.")
            else:
                self.assertTrue(com in ret_o,"check output error.")

    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
