#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_fdisk_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
from socket import timeout
import util_lib
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_fdisk_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    def test(self):
        self.index = util_lib.fdisk_space(self)
        if self.index == 0:
            ret = util_lib.fdisk_add(self)
            self.assertTrue(ret is 0, 'check ret error.')
            util_lib.fdisk_help(self)
        else:
            self.skip('All space for primary partitions is in use')


    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        if self.index == 0:
            ret = util_lib.fdisk_del(self)
            self.assertTrue(ret is not 0, 'check ret error.')
