#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_rename_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    meiyou
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_rename_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("touch foo.txt")

    def test(self):
        self.cmd("rename -o foo bar *")
        ret_c,ret_o = self.cmd("ls")
        self.assertFalse('foo' in ret_o, 'rename file error: newname not exit')
        self.assertTrue('bar' in ret_o, 'rename file error: oldname still exit')

        over_err = "Skipping existing file: `bar.txt'"
        self.cmd("touch foo.txt")
        ret_c,ret_o = self.cmd("rename -o foo bar *", ignore_status = True)
        self.assertTrue(ret_o in over_err, "rename overwrite error")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f bar.txt foo.txt")
