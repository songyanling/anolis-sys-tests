#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_scriptreplay_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import util_lib
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_scriptreplay_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        util_lib.script_fun()
        ret_c, ret_o = self.cmd("scriptreplay hello.time hello.txt 2")
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        ret_c,ret_o = self.cmd("rm -rf hello.time")
        ret_c,ret_o = self.cmd("rm -rf hello.txt")