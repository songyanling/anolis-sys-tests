#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_utmpdump_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_utmpdump_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        ret_c, ret_o = self.cmd("utmpdump /var/run/utmp > /tmp/tmp_output.txt")
        ret_c, ret_o = self.cmd("cp /tmp/tmp_output.txt /tmp/back_tmp_utmp.txt")
        login_str = "[6] [02750] [tty1] [LOGIN   ] [tty1        ] [                    ] [1.1.1.1        ] [2022-08-23T06:59:10,749369+00:00]"
        ret_c, ret_o = self.cmd("echo %s >> /tmp/tmp_output.txt" % login_str)
        ret_c, ret_o = self.cmd("utmpdump -r /tmp/tmp_output.txt > /var/run/utmp")
        ret_c, ret_o = self.cmd("utmpdump /var/run/utmp")
        self.assertTrue( login_str in ret_o, 'check output error.')

    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        ret_c, ret_o = self.cmd("utmpdump -r /tmp/back_tmp_utmp.txt > /var/run/utmp")
        ret_c, ret_o = self.cmd("rm -rf /tmp/tmp_output.txt /tmp/back_tmp_utmp.txt")