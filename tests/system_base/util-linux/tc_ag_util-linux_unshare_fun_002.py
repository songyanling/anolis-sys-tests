#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_unshare_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import pexpect
from common.disk import DiskManager
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_unshare_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        DiskManager.prepare_datadisk(self)
        child = pexpect.spawn('unshare --mount-proc --pid --net --fork /bin/bash')
        child.sendline('umount /mnt/testpoint')
        #index = child.expect([pexpect.TIMEOUT,'inet'],timeout=2)
        ret_c, ret_o = self.cmd("df -h")
        self.assertTrue("/mnt/testpoint" in ret_o,'unshare mount check output error.')

        child.sendline('ps -ef |wc -l')
        index = child.expect([pexpect.TIMEOUT,'4'],timeout=2)
        self.assertTrue(index == 1,'unshare pid check output error.')
    
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        DiskManager.cleanup_datadisk(self,"/tmp/test100.img","/mnt/testpoint")
