#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lscpu_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lscpu_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("chcpu -d 1")
        ret_c, ret_o = self.cmd("lscpu --extended --offline | grep -v ONLINE | wc -l")
        self.assertTrue(int(ret_o) == 1, 'check output error.')

    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("chcpu -e 1,3")
