#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux-user_chsh_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux-user_chsh_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux-user"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        ret_c_list, ret_o_list = self.cmd("chsh -l")
        path_list = ret_o_list.split("\n")
        for shell_path in path_list:
            cmdline = "chsh --shell %s " % shell_path
            self.cmd(cmdline)
            ret_c_check,ret_o_check = self.cmd("cat /etc/passwd | grep ^root")
            check_path = ret_o_check.split(":")[-1]
            self.assertTrue(check_path == shell_path, 'Path setting failure')
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
