#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_swapoff_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest
from common.disk import DiskManager

class Test(LocalTest):
    """
    See tc_ag_util-linux_swapoff_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.data_disk = DiskManager.get_data_disk(self)
        if self.data_disk is None:
            self.skip('There is no data disk for test.')
        self.data_disk = "/dev/%s" % self.data_disk
        DiskManager.create_new_part(self, self.data_disk)
        self.cmd("mkswap %s" % self.data_disk)
        self.cmd("swapon %s" % self.data_disk)
        self.cmd("swapoff %s" % self.data_disk)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        DiskManager.delete_part(self, self.data_disk)
