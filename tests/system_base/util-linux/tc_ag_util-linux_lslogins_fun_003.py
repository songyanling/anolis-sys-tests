#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_lslogins_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_lslogins_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("useradd local_lslogins")
    
    def test(self):
        ret_c, ret_o = self.cmd("lslogins --user-accs")
        self.assertTrue("local_lslogins" in ret_o, 'check output error.')
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("userdel local_lslogins")
