#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_ipcrm_006.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_ipcrm_fun_006.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, ret_o = self.cmd("ipcmk --semaphore 100")
        self.id = ret_o.split(": ", 1)[1]
        ret_c, ret_o = self.cmd("ipcs -s |grep %s |awk '{print $1}'" % self.id)
        ret_c, ret_o = self.cmd("ipcrm --semaphore-key %s" % ret_o)
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
