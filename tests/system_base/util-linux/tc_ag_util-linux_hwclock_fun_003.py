#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_hwclock_003.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    meiyou
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_hwclock_fun_003.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        ret_c, self.ori_systime = self.cmd('date "+%Y-%m-%d %H:%M:%S"')
        ret_c, self.ori_hwtime = self.cmd("hwclock")
    
    def test(self):
        ret_c, self.date_time = self.cmd("hwclock | awk '{print $1 }'")
        self.cmd("hwclock --hctosys")
        self.cmd("date '+%%Y-%%m-%%d %%H:%%M:%%S' | grep %s" %self.date_time)

        set_time = '2022-02-02'
        self.cmd("hwclock --set --date %s" %set_time)
        self.cmd("hwclock --hctosys")
        self.cmd("date '+%%Y-%%m-%%d %%H:%%M:%%S' | grep %s" %set_time)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd('date -s "%s"' % self.ori_systime)
        self.cmd('hwclock --set --date "%s"' %self.ori_hwtime)
