#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_fallocate_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_fallocate_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("fallocate --length 100M /home/test_fallocate.txt")
        ret_c, ret_o = self.cmd("fallocate --collapse-range --length 80m /home/test_fallocate.txt")
        ret_c, ret_o = self.cmd("ls -lh /home/test_fallocate.txt |awk -F \" \" '{print $5}'")
        self.assertTrue("20M" in ret_o, 'check output error.')
        abnormal_situation ={
            "fallocate --collapse-range --length 20m /home/test_fallocate.txt":"fallocate failed",
            "fallocate --collapse-range --length 0 /home/test_fallocate.txt":" invalid length",
            "fallocate --collapse-range --length 1024 /xxxxx/xxxx":"cannot open"
        }
        for  abn in abnormal_situation.keys():
            ret_c, ret_o = self.cmd(abn,ignore_status=True)
            self.assertTrue(abnormal_situation[abn]  in ret_o, 'check output error.')   
    
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /home/test_fallocate.txt")