#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_wipefs_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_wipefs_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        self.cmd("dd if=/dev/zero of=/home/fsck_test.img bs=1M count=100")
        self.cmd("chmod 600 /home/fsck_test.img")
        self.cmd("mkswap -L newswap /home/fsck_test.img")
        self.cmd("swapon  /home/fsck_test.img")
        ret_c, _ = self.cmd("wipefs -a /home/fsck_test.img", ignore_status=True)
        self.assertTrue(ret_c is not 0, "Can not wipefs when fs is swwapon.")
        self.cmd("swapoff  /home/fsck_test.img")
        self.cmd("wipefs -a /home/fsck_test.img")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /home/fsck_test.img")
