#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_findfs_002.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
import subprocess
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_findfs_fun_002.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        ret_c, self.uuid = self.cmd("blkid | head -1 | awk -F 'UUID=' '{print $2'}|awk -F ' ' '{print $1'}")
        ret_c, ret_o = self.cmd("findfs UUID=%s" % self.uuid)
        abnormal_situation ={
            "findfs UUID=":"unable to resolve",
            "findfs UUID=xxxxx":" unable to resolve"
        }
        for abn in abnormal_situation.keys():
            ret_c, ret_o = self.cmd(abn,ignore_status=True)
            self.assertTrue(abnormal_situation[abn]  in ret_o, 'check output error.')  
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
