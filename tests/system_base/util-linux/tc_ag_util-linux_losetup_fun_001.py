#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
@File:      tc_ag_util-linux_losetup_001.py
@Time:      2022/06/20 11:12:50
@Author:    zhangtaibo
@Version:   1.0
@Contact:   zhangtaibo.ztb@alibaba-inc.com
@License:   Mulan PSL v2
@Modify:    liangjian
"""
from common.disk import DiskManager
from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ag_util-linux_losetup_fun_001.yaml for details

    :avocado: tags=fix,P1,noarch,local,ag
    """
    PARAM_DIC = {"pkg_name": "util-linux"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
    
    def test(self):
        data_disk = DiskManager.prepare_datadisk(self,"test.img","/tmp/disk_test")
        ret_c, ret_o = self.cmd("losetup -a")
        self.assertTrue(data_disk in ret_o, 'check output error.') 
        DiskManager.cleanup_datadisk(self,"test.img","/tmp/disk_test")

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
