# -*- encoding: utf-8 -*-

"""
@File:      tc_procps-ng_ps_fun001.py
@Time:      2022/12/27 17:34:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest
import os

class Test(LocalTest):
    """
    See tc_procps-ng_ps_fun001.yaml for details

    :avocado: tags=P1,noarch,local,procps-ng
    """
    PARAM_DIC = {"pkg_name": "procps-ng"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y procps-ng")
        os.system("sleep 1h & echo $! > test_pid")
        code,testprocess_pid = self.cmd("cat test_pid")
        self.pid=testprocess_pid

    def test(self):
        code,ps_result=self.cmd("ps -aux | grep sleep")
        self.assertIn(self.pid,ps_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("kill -9 {}".format(self.pid))
        self.cmd("rm -rf test_pid")