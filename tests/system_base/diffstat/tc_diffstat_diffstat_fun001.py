# -*- encoding: utf-8 -*-

"""
@File:      tc_diffstat_diffstat_fun001.py
@Time:      2023/1/13 10:15:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_diffstat_diffstat_fun001.yaml for details

    :avocado: tags=P1,noarch,local,diffstat
    """
    PARAM_DIC = {"pkg_name": "diffstat"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y diffstat")
        self.cmd("mkdir diffstat_test1 diffstat_test2")
        self.cmd("touch diffstat_test1/test.txt diffstat_test2/test.txt")
        self.cmd("echo abc >>diffstat_test1/test.txt")
        self.cmd("echo 123 >>diffstat_test1/test.txt")
        self.cmd("echo abc >>diffstat_test2/test.txt")
        self.cmd("echo 456 >>diffstat_test2/test.txt")

    def test(self):
        code,bzip2_result=self.cmd("diff diffstat_test1 diffstat_test2 | diffstat")
        self.assertIn("test.txt",bzip2_result)
        self.assertIn("1 insertion(+), 1 deletion(-)",bzip2_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf diffstat_test1 diffstat_test2")