# -*- encoding: utf-8 -*-

"""
@File:      tc_gzip_gzip_fun001.py
@Time:      2022/12/27 17:34:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_gzip_gzip_fun001.yaml for details

    :avocado: tags=P1,noarch,local,gzip
    """
    PARAM_DIC = {"pkg_name": "gzip"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y gzip")
        self.cmd("touch systest_gzip_testfile")
        self.cmd("echo 'gzip test' > systest_gzip_testfile")

    def test(self):
        self.cmd("gzip systest_gzip_testfile")
        self.cmd("gzip -d systest_gzip_testfile.gz")
        code,result=self.cmd("cat systest_gzip_testfile")
        self.assertEquals("gzip test",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f systest_gzip_testfile")