# -*- encoding: utf-8 -*-

"""
@File:      tc_tar_tar_fun001.py
@Time:      2022/12/29 17:30:20
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_tar_tar_fun001.yaml for details

    :avocado: tags=P1,noarch,local,tar
    """
    PARAM_DIC = {"pkg_name": "tar"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y tar")
        self.cmd("mkdir tar_testdir")
        self.cmd("touch tar_testdir/tar_testfile")
        self.cmd("echo 'tar test' > tar_testdir/tar_testfile")

    def test(self):
        self.cmd("tar -zcvf tar_test.tar.gz ./tar_testdir")
        self.cmd("rm -rf tar_testdir")
        self.cmd("tar -zxvf tar_test.tar.gz ./tar_testdir")
        code,result=self.cmd("cat tar_testdir/tar_testfile")
        self.assertEquals("tar test",result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf tar_test.tar.gz tar_testdir")