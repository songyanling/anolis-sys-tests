# -*- encoding: utf-8 -*-

"""
@File:      tc_c++_c++_fun_001.py
@Time:      2023/2/27 
@Author:    sunqingwei
@Version:   1.0
@Contact:   sunqingwei@uniontech.com
@License:   Mulan PSL v2
@Modify:    sunqingwei
"""

from common.basetest import LocalTest


class Test(LocalTest):
    """
    See tc_c++_c++_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "gcc-c++"}

    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('mkdir /tmp/test')
        exc_cmd = r'''cat > /tmp/test/c++.cpp <<EOF
#include <iostream>
using namespace std;
int main()
{
cout << "Hello C++!" << endl;
return 0;
}
EOF
'''

        self.cmd(exc_cmd)

    def test(self):
        self.cmd("g++ /tmp/test/c++.cpp -o /tmp/test/c++")
        code, result = self.cmd("/tmp/test/c++")
        self.assertIn("Hello C++!", result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/test")
