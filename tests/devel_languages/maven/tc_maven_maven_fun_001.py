# -*- encoding: utf-8 -*-

"""
@File:      tc_maven_maven_fun_001.py
@Time:      Tue Sep 19 2023 18:19:31
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""


from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_maven_maven_fun_001.yaml for details

    :avocado: tags=P2,noarch,local,maven
    """
    PARAM_DIC = {"pkg_name": "maven"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd('mkdir -p hello-world')
        self.cmd('mkdir -p hello-world/src/main/java/com/sucsoft')
        cmdline='''cat >hello-world/src/main/java/com/sucsoft/HelloWorld.java<<EOF
public class HelloWorld {
public String sayHello(){
return "Hello World";
}
public static void main(String[] args) {
System.out.println(new HelloWorld().sayHello());
}
}
EOF'''
        self.cmd(cmdline)
        cmdline='''cat >hello-world/pom.xml<<EOF
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
<modelVersion>4.0.0</modelVersion>
<groupId>com.sucsoft</groupId>
<artifactId>hello-world</artifactId>
<version>1.0-SNAPSHOT</version>
<packaging>jar</packaging>
<name>Maven Hello World Project</name>
<dependencies>
<dependency>
<groupId>junit</groupId>
<artifactId>junit</artifactId>
<version>4.7</version>
<scope>test</scope>
</dependency>
</dependencies>
</project>
EOF'''
        self.cmd(cmdline)


    def test(self):
        self.cmd("mvn --version")
        code,result=self.cmd("cd hello-world;mvn compile")
        self.assertIn("SUCCESS", result)
        code,result=self.cmd("cd hello-world;mvn install")
        self.assertIn("SUCCESS", result)
        

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello-world")
