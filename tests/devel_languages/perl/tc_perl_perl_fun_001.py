# -*- encoding: utf-8 -*-

"""
@File:      tc_perl_perl_fun_001.py
@Time:      2023/4/20 15:47:20
@Author:    zhaozhenyang
@Version:   1.0
@Contact:   zhaozhenyang@uniontech.com
@License:   Mulan PSL v2
@Modify:    zhaozhenyang
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_perl_perl_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "perl"}
    
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('perl -v')
        self.assertIn('Copyright 1987-2018, Larry Wall', result)
        code, result = self.cmd('perl -V')
        self.assertIn('Summary of my perl5', result)
        code, result = self.cmd('''perl -e 'print  "Hello World\n"' ''')
        self.assertIn('Hello World', result)
        cmd = '''cat > hello.pl << EOF
print "hello world !\n"
EOF'''
        self.cmd(cmd)
        code, result = self.cmd('perl hello.pl')
        self.assertIn('hello world !', result)
        
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f hello.pl")


