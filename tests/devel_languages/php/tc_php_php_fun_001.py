# -*- encoding: utf-8 -*-

"""
@File:      tc_php_php_fun_001.py
@Time:      2023/09/01 08:57:12
@Author:    fuyahong
@Version:   1.0
@Contact:   fuyahong@uniontech.com
@License:   Mulan PSL v2
@Modify:    fuyahong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_php_php_fun_001.yaml for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "php"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        exc_cmd = '''cat << EOF >> "/tmp/hello.php" 
<?php
    echo "hello php!"
?>
EOF'''
        self.cmd(exc_cmd)
        code,php_result=self.cmd("php /tmp/hello.php")
        print(php_result)
        self.assertIn("hello php!",php_result)
      
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf /tmp/hello.php") 
