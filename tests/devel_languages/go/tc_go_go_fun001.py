# -*- encoding: utf-8 -*-

"""
@File:      tc_go_go_fun001.py
@Time:      2023/1/15 21:57:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_go_go_fun001.yaml for details

    :avocado: tags=P1,noarch,local,golang
    """
    PARAM_DIC = {"pkg_name": "golang"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y golang")
        self.cmd("touch hello.go")
        self.cmd("echo -e 'package main\nimport (\n\"fmt\"\n)\nfunc main() {\nfmt.Println(\"Hello World! Welcome to Go Lang!\")\n}' >> hello.go")

    def test(self):
        self.cmd("go build hello.go")
        code,go_result=self.cmd("./hello")
        self.assertIn("Welcome to Go Lang!",go_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.go")