# -*- encoding: utf-8 -*-

"""
@File:      tc_ruby_ruby_fun002.py
@Time:      Thu Sep 21 2023 10:44:40
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ruby_ruby_fun002.yaml for details

    :avocado: tags=P1,noarch,local,bzip2
    """
    PARAM_DIC = {"pkg_name": "ruby"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        cmdline="""cat > hello.rb <<EOF
puts  'hello world !'  
EOF"""
        self.cmd(cmdline)

    def test(self):
        code,ruby_result=self.cmd("ruby hello.rb")
        self.assertIn("hello world !",ruby_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf hello.rb")