# -*- encoding: utf-8 -*-

"""
@File:      tc_ruby_ruby_fun001.py
@Time:      2023/1/12 16:36:12
@Author:    ylsong
@Version:   1.0
@Contact:   songyl157@chinaunicom.cn
@License:   Mulan PSL v2
@Modify:    ylsong
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_ruby_ruby_fun001.yaml for details

    :avocado: tags=P1,noarch,local,bzip2
    """
    PARAM_DIC = {"pkg_name": "ruby"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)
        self.cmd("yum install -y ruby")
        self.cmd("touch ruby_testfile")
        self.cmd("chmod 750 ruby_testfile")
        self.cmd("echo '#!/usr/bin/ruby' >> ruby_testfile")
        self.cmd("echo 'class Sample' >> ruby_testfile")
        self.cmd("echo 'def hello' >> ruby_testfile")
        self.cmd("echo 'puts \"hello world\"' >> ruby_testfile")
        self.cmd("echo 'end' >> ruby_testfile")
        self.cmd("echo 'end' >> ruby_testfile")
        self.cmd("echo 's = Sample.new' >> ruby_testfile")
        self.cmd("echo 's.hello' >> ruby_testfile")

    def test(self):
        code,ruby_result=self.cmd("which ruby")
        self.assertIn("/usr/bin/ruby",ruby_result)
        code,ruby_result=self.cmd("ruby -v")
        self.assertIn("ruby",ruby_result)
        code,ruby_result=self.cmd("./ruby_testfile")
        self.assertIn("hello world",ruby_result)

    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -rf ruby_testfile")