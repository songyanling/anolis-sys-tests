# -*- encoding: utf-8 -*-

"""
@File:      tc_lua_lua_fun_001.py
@Time:      2023/3/27 15:47:20
@Author:    gaoshuaishuai
@Version:   1.0
@Contact:   gaoshuaishuai@uniontech.com
@License:   Mulan PSL v2
@Modify:    gaoshuaishuai
"""

from common.basetest import LocalTest

class Test(LocalTest):
    """
    See tc_lua_lua_fun_001 for details

    :avocado: tags=P1,noarch,local
    """
    PARAM_DIC = {"pkg_name": "lua"}
    def setUp(self):
        super().setUp(self.PARAM_DIC)

    def test(self):
        code, result = self.cmd('lua -v')
        self.assertIn('Lua.org', result)
        code, result = self.cmd("lua -e'a=666' -e 'print(a)'")
        self.assertIn('666', result)
        cmd = '''cat >  hello.lua << EOF
#!/bin/lua
print("Hello World!")
EOF'''
        self.cmd(cmd)
        code, result = self.cmd('lua hello.lua')
        self.assertIn('Hello World!', result)
    def tearDown(self):
        super().tearDown(self.PARAM_DIC)
        self.cmd("rm -f hello.lua")


